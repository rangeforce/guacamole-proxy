
This project is based on

https://github.com/vadimpronin/guacamole-lite


# Secret

Make sure to set a ENV variables:

* `GUAC_SECRET` for the `AES-256-CBC` cypher. This secret must be accessible for the client accessing the guacamole-light app
* `WEBSOCKET_PORT` for the websocket servers port

# ws to wss


create a new rule for nginx `/etc/nginx/sites-available/default` where `proxy_pass` is your guacamole-lite app address in http

```
location /gml/ {
    proxy_pass ​http://127.0.0.1:1337;
    proxy_http_version 1.1;
    proxy_set_header Upgrade $http_upgrade;
    proxy_set_header Connection "Upgrade";
}
```

and use the endpoint `wss://hostname.com/gml/`

# Guacamole-lite

https://www.npmjs.com/package/guacamole-lite

```
npm install --save bunyan
npm install --save bunyan-syslog

npm install --save guacamole-lite

```
