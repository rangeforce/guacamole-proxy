#!/usr/bin/env node
/*
This project is based on

https://github.com/vadimpronin/guacamole-lite

*/
 
const GuacamoleLite = require('guacamole-lite');

var bunyan = require('bunyan');
var bsyslog = require('bunyan-syslog');
 
var logger = bunyan.createLogger({
    name: 'guacamole-proxy',
    streams: [ {
        level: 'debug',
        type: 'raw',
        stream: bsyslog.createBunyanStream({
            type: 'sys',
            facility: bsyslog.local0,
            host: '127.0.0.1',
            port: 514
        })
    }]
});


const websocketOptions = {
	host: '0.0.0.0',
    port: process.env.WEBSOCKET_PORT // we will accept connections to this port
};
 
const guacdOptions = {
	host: '172.17.0.1',
    port: 4822 // port of guacd
};
 
const clientOptions = {
    crypt: {
        cypher: 'AES-256-CBC',
        key: process.env.GUAC_SECRET
    }
};
 
const guacServer = new GuacamoleLite(websocketOptions, guacdOptions, clientOptions);

guacServer.on('open', (clientConnection) => {
    //const url = 'http://our-backend-server/api/connection/open?userId=' + clientConnection.connectionSettings.userId
    
    //Http.request(url).end();
    logger.info('opened', clientConnection.connectionSettings)
});
 
guacServer.on('close', (clientConnection) => {
    //const url = 'http://our-backend-server/api/connection/close?userId=' + clientConnection.connectionSettings.userId
    
    //Http.request(url).end();
   logger.info('closed', clientConnection.connectionSettings)
});
 
guacServer.on('error', (clientConnection, error) => {
    logger.error(clientConnection, error);
});

